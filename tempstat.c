/*
 * Originally from
 * https://github.com/silverfisk/home-automation/blob/master/temperv14/temperv14.c
 * 
 * See comment-doc.txt for some pseudo-changelog stuff.
 */

#include <usb.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#include <sysexits.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#define VENDOR_ID  0x0c45
#define PRODUCT_ID 0x7401

#define INTERFACE1 0x00
#define INTERFACE2 0x01

const static int reqIntLen = 8;
const static int reqBulkLen = 8;
const static int endpoint_Int_in = 0x82;	/* endpoint 0x81 address for IN */
const static int endpoint_Int_out = 0x00;	/* endpoint 1 address for OUT */
const static int endpoint_Bulk_in = 0x82;	/* endpoint 0x81 address for IN */
const static int endpoint_Bulk_out = 0x00;	/* endpoint 1 address for OUT */
const static int timeout = 5000;	/* timeout in ms */

const static char uTemperature[] =
    { 0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00 };
const static char uIni1[] =
    { 0x01, 0x82, 0x77, 0x01, 0x00, 0x00, 0x00, 0x00 };
const static char uIni2[] =
    { 0x01, 0x86, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00 };

static int debug = 0;
static int repeats = 0;

extern char version[];

void
bad(const char *why)
{
	fprintf(stderr, "Fatal error> %s\n", why);
	exit(17);
}

usb_dev_handle *find_lvr_winusb();

void
usb_detach(usb_dev_handle * lvr_winusb, int iInterface)
{
	int ret;

	ret = usb_detach_kernel_driver_np(lvr_winusb, iInterface);
	if (ret) {
		if (errno == ENODATA) {
			if (debug) {
				printf("Device already detached\n");
			}
		}
		else {
			if (debug) {
				printf("Detach failed: %s[%d]\n",
				       strerror(errno), errno);
				printf("Continuing anyway\n");
			}
		}
	}
	else {
		if (debug) {
			printf("detach successful\n");
		}
	}
}

usb_dev_handle *
setup_libusb_access()
{
	usb_dev_handle *lvr_winusb;

	if (debug) {
		usb_set_debug(255);
	}
	else {
		usb_set_debug(0);
	}
	usb_init();
	usb_find_busses();
	usb_find_devices();

	if (!(lvr_winusb = find_lvr_winusb())) {
		printf("Couldn't find the USB device, Exiting\n");
		return NULL;
	}

	usb_detach(lvr_winusb, INTERFACE1);

	usb_detach(lvr_winusb, INTERFACE2);

	if (usb_set_configuration(lvr_winusb, 0x01) < 0) {
		printf("Could not set configuration 1\n");
		return NULL;
	}
       // Microdia tiene 2 interfaces
	if (usb_claim_interface(lvr_winusb, INTERFACE1) < 0) {
		printf("Could not claim interface\n");
		return NULL;
	}

	if (usb_claim_interface(lvr_winusb, INTERFACE2) < 0) {
		printf("Could not claim interface\n");
		return NULL;
	}

	return lvr_winusb;
}

usb_dev_handle *
find_lvr_winusb()
{

	struct usb_bus *bus;
	struct usb_device *dev;

	for (bus = usb_busses; bus; bus = bus->next) {
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == VENDOR_ID &&
			    dev->descriptor.idProduct == PRODUCT_ID) {
				usb_dev_handle *handle;

				if (debug) {
					printf
					    ("lvr_winusb with Vendor Id: %x and Product Id: %x found.\n",
					     VENDOR_ID, PRODUCT_ID);
				}

				if (!(handle = usb_open(dev))) {
					printf("Could not open USB device\n");
					return NULL;
				}
				return handle;
			}
		}
	}
	return NULL;
}

void
ini_control_transfer(usb_dev_handle * dev)
{
	int r, i;

	char question[reqIntLen];

    memset(question, 0x01, reqIntLen);

	r = usb_control_msg(dev, 0x21, 0x09, 0x0201, 0x00, (char *)question,
			    2, timeout);
	if (r < 0) {
		perror("USB control write");
		bad("USB write failed");
	}

	if (debug) {
		for (i = 0; i < reqIntLen; i++)
			printf("%02x ", question[i] & 0xFF);
		printf("\n");
	}
}

void
control_transfer(usb_dev_handle * dev, const char *pquestion)
{
	int r, i;

	char question[reqIntLen];

	memcpy(question, pquestion, sizeof question);

	r = usb_control_msg(dev, 0x21, 0x09, 0x0200, 0x01, (char *)question,
			    reqIntLen, timeout);
	if (r < 0) {
		perror("USB control write");
		bad("USB write failed");
	}

	if (debug) {
		for (i = 0; i < reqIntLen; i++)
			printf("%02x ", question[i] & 0xFF);
		printf("\n");
	}
}

void
interrupt_transfer(usb_dev_handle * dev)
{

	int r, i;
	char answer[reqIntLen];
	char question[reqIntLen];

	for (i = 0; i < reqIntLen; i++)
		question[i] = i;
	r = usb_interrupt_write(dev, endpoint_Int_out, question, reqIntLen,
				timeout);
	if (r < 0) {
		perror("USB interrupt write");
		bad("USB write failed");
	}
	r = usb_interrupt_read(dev, endpoint_Int_in, answer, reqIntLen,
			       timeout);
	if (r != reqIntLen) {
		perror("USB interrupt read");
		bad("USB read failed");
	}

	if (debug) {
		for (i = 0; i < reqIntLen; i++)
			printf("%i, %i, \n", question[i], answer[i]);
	}

	usb_release_interface(dev, 0);
}

void
interrupt_read(usb_dev_handle * dev)
{

	int r, i;
	char answer[reqIntLen];

	bzero(answer, reqIntLen);

	r = usb_interrupt_read(dev, 0x82, answer, reqIntLen, timeout);
	if (r != reqIntLen) {
		perror("USB interrupt read");
		bad("USB read failed");
	}

	if (debug) {
		for (i = 0; i < reqIntLen; i++)
			printf("%02x ", answer[i] & 0xFF);

		printf("\n");
	}
}

void
interrupt_read_temperature(usb_dev_handle * dev, float *tempC)
{

	int r, i, temperature;
	char answer[reqIntLen];

	bzero(answer, reqIntLen);

	r = usb_interrupt_read(dev, 0x82, answer, reqIntLen, timeout);
	if (r != reqIntLen) {
		perror("USB interrupt read");
		bad("USB read failed");
	}

	if (debug) {
		for (i = 0; i < reqIntLen; i++)
			printf("%02x ", answer[i] & 0xFF);

		printf("\n");
	}

	temperature = (answer[3] & 0xFF) + (answer[2] << 8);
	*tempC = temperature * (125.0 / 32000.0);

}

void
bulk_transfer(usb_dev_handle * dev)
{

	int r, i;
	char answer[reqBulkLen];

	r = usb_bulk_write(dev, endpoint_Bulk_out, NULL, 0, timeout);
	if (r < 0) {
		perror("USB bulk write");
		bad("USB write failed");
	}
	r = usb_bulk_read(dev, endpoint_Bulk_in, answer, reqBulkLen, timeout);
	if (r != reqBulkLen) {
		perror("USB bulk read");
		bad("USB read failed");
	}

	if (debug) {
		for (i = 0; i < reqBulkLen; i++)
			printf("%02x ", answer[i] & 0xFF);
	}

	usb_release_interface(dev, 0);
}

void
ex_program(int sig)
{
	repeats = 0;
	(void)signal(SIGINT, SIG_DFL);
}

float
conv_f(float c) 
{
	return (c * 1.8) + 32.0;
}

float
conv_k(float c) 
{
	return c + 273.15;
}


void
usage(const char *program)
{
	char *p;

	if ((p = strchr(program, '/')) != NULL)
		p++;
	else
		p = (char *)program;

	printf("temper version %s\n", version);
	printf("usage: %s [opts] [-f format] [-c calibration(*C)] [interval [repeats]]\n", p);
	printf("opts:\n");
	printf("    -v verbose\n");
	printf("    -m use MRTG format\n");
}


int
fmt_c(char *dst, int max, float t)
{
	snprintf(dst, max, "%.2f", t);
	return strlen(dst);
}


int
fmt_f(char *dst, int max, float t)
{
	t = (1.8 * t) + 32;
	snprintf(dst, max, "%.2f", t);
	return strlen(dst);
}


int
fmt_k(char *dst, int max, float t)
{
	t = t + 273.15;
	snprintf(dst, max, "%.2f", t);
	return strlen(dst);
}


int
fmt_t(char *dst, int max, char *fmt)
{
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	strftime(dst, max, fmt, tm);
	return strlen(dst);
}


char *
braces(char **srcp)
{
	char *src;
	char *end;
	char *result;

	if (srcp == NULL || *srcp == NULL)
		return NULL;
	src = *srcp;

	if (*src != '{')
		return NULL;

	end = strchr(src, '}');
	if (end == NULL)
		return NULL;

	result = malloc(end - src);
	strncpy(result, src+1, end-src-1);
	result[end-src-1] = '\0';
	*srcp = end;
	return result;
}


char *
formatstr(const char *format, float tempc)
{
#define MAXSIZE 1024
	char out[MAXSIZE];
	char *src;
	int len = 0;

	src = (char *)format;
	while (src && *src && len < (MAXSIZE-1)) {
		if (*src == '%') {
			src++;
			if (*src == 'c')
				len += fmt_c(&out[len], MAXSIZE-len-1, tempc);
			else if (*src == 'f')
				len += fmt_f(&out[len], MAXSIZE-len-1, tempc);
			else if (*src == 'k')
				len += fmt_k(&out[len], MAXSIZE-len-1, tempc);
			else if (*src == 't') {
				char *tfmt;
				src += 1;
				tfmt = braces(&src);
				if (tfmt) {
					len += fmt_t(&out[len], MAXSIZE-len-1, tfmt);
					free(tfmt);
				}
				else {
					if (len < MAXSIZE-1)
						out[len++] = '%';
					if (len < MAXSIZE-1)
						out[len++] = 't';
				}
			}
			else {
				if (len < MAXSIZE-1)
					out[len++] = '%';
				if (len < MAXSIZE-1)
					out[len++] = *src;
			}
		}
		else {
			out[len++] = *src;
		}
		src++;
	}
	out[len] = '\0';
	return strdup(out);
}


int
main(int argc, char **argv)
{
	usb_dev_handle *lvr_winusb = NULL;
	float tempc;
	int c;
	char *format = "%c at %t{%c}";
	float calibration = 0.0;
	int interval = 5;
	char *s;

	while ((c = getopt(argc, argv, "mf:c:vh")) != -1) {
		switch (c) {
		case 'v':
			debug = 1;
			break;

		case 'f':
			format = optarg;
			break;

		case 'm':
			format = "%.2c\n%.2c\n%t{%H:%M}\npcsensor";
			break;

		case 'c':
			calibration = atof(optarg);
			break;

		case 'h':
			usage(argv[0]);
			exit(EX_OK);

		default:
			fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			exit(EX_USAGE);
		}
	}

	if (argc > optind) {
		interval = atoi(argv[optind++]);
		repeats = -999;
	}

	if (argc > optind)
		repeats = atoi(argv[optind++]);

	if (argc > optind) {
		fprintf(stderr, "error: too many arguments\n");
		usage(argv[0]);
		exit(EX_USAGE);
	}

	if ((lvr_winusb = setup_libusb_access()) == NULL) {
		exit(EX_OSERR);
	}

	(void)signal(SIGINT, ex_program);

	ini_control_transfer(lvr_winusb);

	control_transfer(lvr_winusb, uTemperature);
	interrupt_read(lvr_winusb);

	control_transfer(lvr_winusb, uIni1);
	interrupt_read(lvr_winusb);

	control_transfer(lvr_winusb, uIni2);
	interrupt_read(lvr_winusb);
	interrupt_read(lvr_winusb);

	do {
		control_transfer(lvr_winusb, uTemperature);
		interrupt_read_temperature(lvr_winusb, &tempc);
		tempc += calibration;
		s = formatstr(format, tempc);
		puts(s);
		free(s);

		if (repeats == -999 || repeats > 1)
			sleep(interval);
	} while (repeats == -999 || --repeats > 0);

	usb_release_interface(lvr_winusb, INTERFACE1);
	usb_release_interface(lvr_winusb, INTERFACE2);

	usb_close(lvr_winusb);

	return 0;
}
