all:	tempstat

CFLAGS = -g -Wall
LDFLAGS = -g
VERSION = v0.2

version.c:
	./git-id -c $(VERSION) >version.c

tempstat:	tempstat.o version.o
	$(CC) -DUNIT_TEST -o $@ $^ -lusb

clean:		
	rm -f tempstat *.o version.c

rules-install:			# must be superuser to do this
	cp 99-tempsensor.rules /etc/udev/rules.d
